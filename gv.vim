sign define add text=+ texthl=DiffAdd
call prop_type_add('deletion', {'highlight': 'DiffDelete'})

function! gv#init()
    if !exists('b:gvstate')
        let b:gvstate = {
                    \"buffile": tempname(),
                    \"markers": {}}
    endif
    return b:gvstate
endfunction

function! gv#marker(obj, content)
    let l:id = a:obj.type . a:obj.line . a:content

    if has_key(b:gvstate.markers, l:id)
        return l:id
    else
        let b:gvstate.markers[l:id] = a:obj
    endif

    if a:obj.type == '+'
        call sign_place(a:obj.line, "gv", "add", bufname(), {"lnum": a:obj.line})
    elseif a:obj.type == '-'
        call prop_add(a:obj.line, 0, {'type': 'deletion', 'text_align': 'below', 'text': a:content})
    endif

    return l:id
endfunction

function! gv#unmarker(id)
    let l:obj = remove(b:gvstate.markers, a:id)
    if l:obj.type == '+'
        call sign_unplace("gv", {"buffer": bufname(), "id": l:obj.line})
    elseif l:obj.type == '-'
        call prop_remove({"type": "deletion"}, l:obj.line)
    endif
endfunction

function! gv#parsediff(diff)
    let l:old = -1
    let l:new = -1

    let l:ids = {}

    for l:line in split(a:diff, "\n")
        " Check if we are looking at a hunk header
        let l:match = matchlist(l:line, "^@@ -\\(\\d\\+\\),\\?\\d* +\\(\\d\\+\\),\\?\\d* @@")
        if !empty(l:match)
            let l:old = l:match[1]
            let l:new = l:match[2]
            continue
        endif

        if l:old < 0 || l:new < 0
            continue
        endif

        if l:line[0] == ' '
            let l:old = l:old + 1
            let l:new = l:new + 1
        elseif l:line[0] == '+'
            let l:id = gv#marker({"line": l:new, "type": '+'}, l:line[1:])
            let l:ids[l:id] = +1

            let l:new = l:new + 1
        elseif l:line[0] == '-'
            let l:id = gv#marker({"line": l:new, "type": '-'}, l:line[1:])
            let l:ids[l:id] = +1

            let l:old = l:old + 1
        endif
    endfor

    for l:key in filter(keys(b:gvstate.markers), '!has_key(l:ids, v:val)')
        call gv#unmarker(l:key)
    endfor
endfunction

function! gv#unplacesign()
    call sign_unplace("gv")
    call prop_remove({"type": "deletion"}, 1, line("$"))
endfunction

function gv#placegit()
    call gv#init()

    let l:buf = join(getbufline(bufname(), 1, '$'),"\n")
    call system("git show HEAD:./" .. expand("%p") .. " > " .. b:gvstate.buffile)
    let l:diff = system("diff -U0 " .. b:gvstate.buffile .. " -", l:buf)
    call gv#parsediff(l:diff)
endfunction

autocmd TextChanged <buffer> call gv#placegit()
call gv#placegit()
